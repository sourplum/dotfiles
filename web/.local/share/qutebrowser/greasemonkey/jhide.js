// ==UserScript==
// @name JHide Content Blocking
// @run-at document-end
// @qute-js-world user
// ==/UserScript==

const exceptions = {};
const hits = {"threadreaderapp.com":[".sharingfooter"],"www.instagram.com":["._Yhr4.RnEpo"],"scantrad.net":["#selectPg",".asqx","#topLEL",".qsdqsdqsdj_left.qsdqsdqsdj"],"mangadex.org":["#announcement",".social-media-btns"],"www.cmso.com":["#entete-wrapper"],"nyaa.si":["[href=\"https:\/\/twitter.com\/NyaaV2\"]","[href=\"\/\/sukebei.nyaa.si\"]","[href=\"\/\/sukebei.Nyaa.si\"]"],"mon.cmso.com":[".is-desktop",".c-nav--noborderbottom.c-nav--sociaux.c-nav > .c-nav__list"],"www.wikiart.org":[".short-of-the-month-section > header",".movie-section",".movie-section.main-info > aside","li.root:nth-of-type(5) > [href=\"javascript:;\"] > h4","li.root:nth-of-type(4) > [href=\"javascript:;\"] > h4","[href=\"\/en\/artists-by-century\/21\"]",".artists.wiki-footer-column-links > li:nth-of-type(11)",".shop.wiki-footer-column-links",".court-metrage.wiki-footer-column-links",".shop.wiki-footer-column-links > li > .hidden-blank",".ng-valid-pattern.ng-valid.ng-pristine.form-subscribe",".court-metrage.wiki-footer-column-links > li > [href=\"\/en\/app\/court-metrage\"]","[href=\"\/en\/female-artists\"]","li.root.selected:nth-of-type(4) > [href=\"javascript:;\"] > h4","[href=\"\/en\/app\/court-metrage\"] > h5",".view-painting-section.short-of-the-month-section"],"fr.ulule.com":["#old-browser-banner","#old-browser-banner > p"],"wordgrammar.net":["#ai_widget-4"],"fr.aliexpress.com":[".ams-region","div.may-like:nth-of-type(4)",".product-fix-wrap",".page-container",".global-sale-firstscreen.home-firstscreen"],"www.jeuxdescartesbordeaux.com":["#sdgdpr_modal_wrapper"]};
const fallback = null;
const whitelist = {"null":true};
/** Set union of A and B. Destroys set A. */
function setUnion(a, b) {
    b.forEach(function (x) {
        __PS_MV_REG = [];
        return a.add(x);
    });
    __PS_MV_REG = [];
    return a;
};
/** Set difference of A and B. Destroys set A. */
function setDifference(a, b) {
    b.forEach(function (x) {
        __PS_MV_REG = [];
        return a['delete'](x);
    });
    __PS_MV_REG = [];
    return a;
};
/**
 * Convert this iterable into a css string.
 * If UNDO, unset the css instead of setting it.
 */
function cssFromIter(iter, undo) {
    __PS_MV_REG = [];
    return Array.from(iter).join(',').concat(undo ? '{display:unset !important;}' : '{display:none !important;}');
};
/** Split domain into sections. google.com -> (google.com com) */
function splitDomain() {
    __PS_MV_REG = [];
    return (function () {
        var collecting5 = [];
        var FIRST6 = true;
        for (var domain = window.location.hostname; true; domain = domain) {
            var domainIndex = domain.indexOf('.');
            var domain = FIRST6 ? domain : domain.substring(domainIndex + 1);
            if (domainIndex < 0) {
                break;
            };
            collecting5.push(domain);
            FIRST6 = null;
        };
        __PS_MV_REG = [];
        return collecting5;
    })();
};
domainList = splitDomain();
isException = domainList.some(function (d) {
    __PS_MV_REG = [];
    return Boolean(whitelist[d]);
});
if (!isException) {
    GM_addStyle('{display:none !important;}');
    (function () {
        var exceptionsInFlight = new Set();
        var rulesInFlight = new Set();
        var finalRules = new Set(fallback);
        for (var domain = null, _js_idx8 = 0; _js_idx8 < domainList.length; _js_idx8 += 1) {
            domain = domainList[_js_idx8];
            var current = exceptions[domain];
            if (current) {
                setUnion(exceptionsInFlight, current.filter(function (x) {
                    return !rulesInFlight.has(x);
                }));
            };
            var current9 = hits[domain];
            if (current9) {
                setUnion(rulesInFlight, current9.filter(function (x) {
                    return !exceptionsInFlight.has(x);
                }));
            };
        };
        setDifference(finalRules, exceptionsInFlight);
        setUnion(finalRules, rulesInFlight);
        if (0 !== finalRules.size) {
            GM_addStyle(cssFromIter(finalRules));
        };
        if (0 !== exceptionsInFlight.size) {
            GM_addStyle(cssFromIter(exceptionsInFlight, true));
        };
        __PS_MV_REG = [];
        return null;
    })();
};

