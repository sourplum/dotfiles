#!/usr/bin/env sh

normal=$(tput sgr0)                      # normal text
#normal=$'\e[0m'                           # (works better sometimes)
bold=$(tput bold)                         # make colors bold/bright
red="$bold$(tput setaf 1)"                # bright red text
green=$(tput setaf 2)                     # dim green text
fawn=$(tput setaf 3); beige="$fawn"       # dark yellow text
yellow="$bold$fawn"                       # bright yellow text
darkblue=$(tput setaf 4)                  # dim blue text
blue="$bold$darkblue"                     # bright blue text
purple=$(tput setaf 5); magenta="$purple" # magenta text
pink="$bold$purple"                       # bright magenta text
darkcyan=$(tput setaf 6)                  # dim cyan text
cyan="$bold$darkcyan"                     # bright cyan text
gray=$(tput setaf 7)                      # dim white text
darkgray="$bold"$(tput setaf 0)           # bold black = dark gray text
white="$bold$gray"                        # bright white text

pprint() {
    icon=" "
    echo "${cyan}${icon} ${1}${normal}"
}

service() {
    pprint "Activating service $1..."
    [ ! -L "/var/service/$1" ] && sudo ln -s "/etc/sv/$1/" /var/service/
}

pull() {
    cd "$1" || return
    pprint "Updating ${1##*/}"
    git pull
}

pkgs() {
    pprint "Installing packages from group $1..."
    list=$(yq -r ."$1| join(' ')" packages.yaml)
    sudo xbps-install -S "$list"
}

_pkgs() {
    pprint "Installing custom packages from group $1..."
    packagedir=$2
    packagelnk=$3
    [ ! -d "$packagedir" ] && mkdir -p "$packagedir"

    list=$(yq -cr ."$1[]" packages.yaml)
    for row in $list; do
        _jq() {
            echo "${row}" | jq -r "${1}"
        }
        packagename=$(_jq '.name'); packageurl=$(_jq '.url'); packagebin=$(_jq '.bin')
        [ ! -d "$packagedir/$packagename" ] && git clone "$packageurl" "$packagedir/$packagename" || pull "$packagedir/$packagename"
        [ -d "$packagelnk" ] && [ ! -L "$packagelnk/$packagename" ] && ln -s "$packagedir/$packagebin" "$packagelnk/$packagename"
    done
}

vimplug() {
    [ ! -f "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/autoload/plug.vim" ] &&
        echo "${cyan}Installing vim plug..." &&
        curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/autoload/plug.vim" --create-dirs "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
}

terminal() {
    pkgs terminal
    stow terminal
    _pkgs "_terminal" "$HOME/bin/src" "$HOME/bin"
    vimplug
}

menu() {
    pkgs menu
    stow menu
}

bspwm() {
    pkgs bspwm
    go get github.com/cmschuetz/btops
    stow bspwm
    service dbus
    service ldm
    [ ! -f "$HOME/.cache/wal/sequences" ] && wal -i ./.pictures/default-wallpaper.png
}

music() {
    pkgs music
    stow music
    service alsa
    service sndiod
}

web() {
    pkgs web
    _pkgs "_web" "$HOME/.config/qutebrowser"
    stow web
    service privoxy
    service tor
    service dnscrypt-proxy
}

qtapps() {
    pkgs qtapps
}

videocardati() {
    pkgs videocardati
}

game() {
    [ ! -f "/etc/xbps.d/10-repository-multilib.conf" ] && [ ! -f "/etc/xbps.d/10-repository-multilib-nonfree.conf" ] &&
        pprint "Activating multilib repository..." &&
        sudo xbps-install -Syv void-repo-multilib void-repo-multilib-nonfree
    pkgs game
}

base() {
    pkgs base
}

devops() {
    pkgs devops
}

voidrepo() {
    [ ! -f "/etc/xbps.d/my-local-repo.conf" ] && [ -d "$HOME/.void-packages" ] &&
        pprint "Add local repository to list of xbps remote hosts..." &&
        echo "repository=$HOME/.void-packages/hostdir/binpkgs/neptune" | sudo tee "/etc/xbps.d/my-local-repo.conf" ||
        pprint "Cloning custom void-packages repository..." &&
        git clone 'https://git.neptune.one/void/void-packages.git' "$HOME/.void-packages" && void
}

printrecipes() {
    echo "Available recipes:
      base         [compilers, kernel and base tools]
      bspwm        [window manager]
      devops       [toolbox for automation and containers]
      game         [play games on GNU/Linux]
      qtapps       [gui apps]
      menu         [launcher]
      music        [music, video and sound]
      terminal     [almost everything in the terminal]
      videocardati [owner of ATI video cards only]
      voidrepo     [setup custom void packages]
    爵 web          [browse the web like vim]
    "
}

main() {
    [ ! -x "/bin/stow" ] && sudo xbps-install -S stow
    [ ! -x "/bin/yq" ] && sudo xbps-install -S yq jq

    case $1 in
        --list)
            printrecipes;;
        bspwm)
            bspwm;;
        menu)
            menu;;
        qtapps)
            qtapps;;
        terminal)
            terminal;;
        videocardati)
            videocardati;;
        web)
            web;;
        game)
            game;;
        base)
            base;;
        devops)
            devops;;
        voidrepo)
            voidrepo;;
        *);;
    esac
}

main "${1:-'--list'}"

