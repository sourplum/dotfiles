# vi: set ft=conf :
# https://kokomins.wordpress.com/2019/10/14/mpv-config-guide/

#########
# audio #
#########

# Specify default audio driver (see --ao=help for a list).
ao=pipewire

#pipewire-buffer=47

#########
# video #
#########

vo=gpu
profile=gpu-hq
vd-lavc-dr=yes
opengl-pbo=yes

drm-vrr-enabled=yes

gpu-api=vulkan
vulkan-async-compute=yes
vulkan-async-transfer=yes
vulkan-queue-count=1

icc-profile="~~/icc/AOC C24G1.icm"

###########################
# Vsync and smooth motion #
###########################

#video-sync=display-resample
interpolation=yes
tscale=oversample
interpolation-threshold=-1
## wayland-disable-vsync=yes

#################
# VIDEO OPTIONS #
#################

deband=yes
deband-iterations=5 # Range 1-16. Higher = better quality but more GPU usage. >5 is redundant.
deband-threshold=35 # Range 0-4096. Deband strength.
deband-range=20 # Range 1-64. Range of deband. Too high may destroy details.
deband-grain=5 # Range 0-4096. Inject grain to cover up bad banding, higher value needed for poor sources.
#target-prim=cie1931
#target-trc=bt.1886
#tone-mapping=mobius
#tone-mapping-desaturate=0.0
#hdr-compute-peak=yes
#vf=format=primaries=cie1931:colorlevels=full:convert=yes
#hls-bitrate=max

##########
# shader #
##########

glsl-shaders="~~/shaders/Anime4K_Clamp_Highlights.glsl:~~/shaders/Anime4K_Restore_CNN_VL.glsl:~~/shaders/Anime4K_Upscale_CNN_x2_VL.glsl:~~/shaders/Anime4K_AutoDownscalePre_x2.glsl:~~/shaders/Anime4K_AutoDownscalePre_x4.glsl:~~/shaders/Anime4K_Upscale_CNN_x2_M.glsl"

scale=spline36
cscale=spline36
#cscale=ewa_lanczossharp
dscale=mitchell
dither-depth=auto
correct-downscaling=yes
linear-downscaling=yes
sigmoid-upscaling=yes

##########
# option #
##########

input-default-bindings=no

osc=no
osd-bar=no
border=no

screenshot-format=png
screenshot-png-compression=8
save-position-on-quit

ytdl=yes
ytdl-format=bestvideo*[height<=1080]+bestaudio/best
script-opts=ytdl_hook-ytdl_path=/home/jojo/bin/yt-dlp

#Cursor hide in ms
cursor-autohide=100

[gif]
profile-cond=filename:match"[.]gif$" ~= nil
loop-file=inf

[720p]
profile-desc=Low quality
profile-cond=width <= 1280 and height <= 720
glsl-shaders="~~/shaders/Anime4K_Clamp_Highlights.glsl:~~/shaders/Anime4K_Restore_CNN_Soft_VL.glsl:~~/shaders/Anime4K_Upscale_CNN_x2_VL.glsl:~~/shaders/Anime4K_AutoDownscalePre_x2.glsl:~~/shaders/Anime4K_AutoDownscalePre_x4.glsl:~~/shaders/Anime4K_Restore_CNN_Soft_M.glsl:~~/shaders/Anime4K_Upscale_CNN_x2_M.glsl"
# glsl-shaders="~~/shaders/Anime4K_Clamp_Highlights.glsl:~~/shaders/Anime4K_Restore_CNN_Soft_VL.glsl:~~/shaders/Anime4K_Upscale_CNN_x2_VL.glsl:~~/shaders/Anime4K_AutoDownscalePre_x2.glsl:~~/shaders/Anime4K_AutoDownscalePre_x4.glsl:~~/shaders/Anime4K_Upscale_CNN_x2_M.glsl"
