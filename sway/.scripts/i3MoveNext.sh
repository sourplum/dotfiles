#!/usr/bin/env sh

wsNext=$(( $( swaymsg -t get_workspaces | jq '.[] | select(.focused).num' ) + $1))
swaymsg move container to workspace $wsNext
swaymsg workspace $wsNext
