" Plug manage all the plugins
" https://vimawesome.com to find new plugins
" install all plugins with
"       :source %
"       :PlugInstall
call plug#begin('~/.local/share/nvim/plugged')
" -----------------------------------------------------------------------------
" COMPATIBILITY
" -----------------------------------------------------------------------------
" vim 8 ameliorations, dependency of other cool programs
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
" -----------------------------------------------------------------------------
" MISCELLANEOUS
" -----------------------------------------------------------------------------
" Close open parenthesis, brackets
"       like so ([])
Plug 'Raimondi/delimitMate'
" select text with :NR to open it in new buffer
" when modif done goes back to main document
Plug 'chrisbra/NrrwRgn'
" Open file with cursor in last position
Plug 'vim-scripts/lastpos.vim'
" Add colors to colors
"       :ColorToggle to see the colors like #80CBC4
Plug 'chrisbra/colorizer', { 'on': 'ColorToggle' }
" Focus mode
" Plug 'junegunn/goyo.vim', { 'for': 'markdown' }
" -----------------------------------------------------------------------------
" SPELLING
" -----------------------------------------------------------------------------
" Write good text, correct spelling errors
Plug 'reedes/vim-lexical'
" Synonymous with
Plug 'ron89/thesaurus_query.vim'
" -----------------------------------------------------------------------------
" GIT
" -----------------------------------------------------------------------------
" Use git log, to revert changes, the easy way
"       :GundoShow to display git time machine
Plug 'sjl/gundo.vim', { 'on': 'GundoShow' }
" -----------------------------------------------------------------------------
" CODE GENERAL
" -----------------------------------------------------------------------------
" syntax to most config, code files
Plug 'sheerun/vim-polyglot'
" Linters
Plug 'w0rp/ale'
" Completion
Plug 'shougo/neoinclude.vim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-go', {'build': {'unix': 'make'}}
"Plug 'zchee/deoplete-clang'
" Plug 'Valloric/YouCompleteMe', { 'do': './install.py --go-completer' }
" Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
" Comments
Plug 'vim-scripts/DoxygenToolkit.vim'
" -----------------------------------------------------------------------------
" PROGRAMMING LANGUAGES
" -----------------------------------------------------------------------------
" Golang
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
" Python
" Plug 'klen/python-mode', { 'for': 'python' }
" Rust
"Plug 'rust-lang/rust.vim', { 'for': 'rust' }
"Plug 'mattn/webapi-vim', { 'for': 'rust' }
"Plug 'racer-rust/vim-racer', { 'for': 'rust' }
" Markdown
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
" Plug 'iamcco/markdown-preview.vim', { 'for': 'markdown' }
Plug 'mzlogin/vim-markdown-toc', { 'for': 'markdown' }
" Asciidoc
Plug 'habamax/vim-asciidoctor'
" Nginx
Plug 'chr4/nginx.vim'
" Logstash
Plug 'robbles/logstash.vim'
" C++
Plug 'octol/vim-cpp-enhanced-highlight', { 'for': 'cpp' }
" OrgMode
Plug 'jceb/vim-orgmode'
" -----------------------------------------------------------------------------
" OPEN A FILE
" -----------------------------------------------------------------------------
" Save and open working session
"       :SaveSession foobar
"       :OpenSession foobar
Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'
" Fuzzy Finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Open a file manager inside vim
" Plug 'francoiscabrol/ranger.vim', { 'on': 'Ranger' }
" Side panel with tree
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'Xuyuanp/nerdtree-git-plugin', { 'on': 'NERDTreeToggle' }
" -----------------------------------------------------------------------------
" MOVE
" -----------------------------------------------------------------------------
" surround a word with ([{}])
" press ysiw] to have foobar be [foobar]
" press cs]{ to have [foobar] be {foobar}
" press VS{ to select a line and add {} arround
Plug 'tpope/vim-surround'
" This plugin adds ic, ac, iC, and aC as text-objects. Use them in commands like vic, cic, and daC
" faster operations on blocks of text
Plug 'coderifous/textobj-word-column.vim'
" press \\b and \\w then press visible key to move to cursor
Plug 'Lokaltog/vim-easymotion'
" align those commas, quotes, tables, config files
Plug 'junegunn/vim-easy-align'
" -----------------------------------------------------------------------------
" Eye Candy
" -----------------------------------------------------------------------------
" The bottom bar
Plug 'itchyny/lightline.vim'
" Color Schemes
Plug 'joshdick/onedark.vim'
" Plug 'morhetz/gruvbox'
" Plug 'chriskempson/base16-vim'
" Plug 'skwp/vim-colors-solarized'
call plug#end()

let g:session_autosave = 'no'
let g:session_autoload = 'no'

let g:ranger_map_keys = 0
let g:vim_markdown_folding_disabled = 1
let g:mkdp_path_to_chrome = "qutebrowser"

" -----------------------------------------------------------------------------
" OPEN A FILE (3 ways)
" -----------------------------------------------------------------------------

" Display project files with side panel
" Press F12 to toggle
nnoremap <F12> :NERDTreeToggle<CR>
let g:NERDTreeRespectWildIgnore = 1

" File manager to open file
" Press F4 to open, q to close
" map <F4> :Ranger<CR>.

" Fuzzy Finder on files and buffer
" Use this everytime, press \f or \b
" master race only
nmap <leader>b :Buffers<CR>
nmap <leader>f :Files<CR>

" -----------------------------------------------------------------------------
" Code completion
" -----------------------------------------------------------------------------

" Youcompleteme
" Code completion, will appears when needed
"if !exists("g:ycm_semantic_triggers")
"  let g:ycm_semantic_triggers = {}
"endif

" Deoplete is cool too, works well with YCM
let g:deoplete#sources#clang#libclang_path= '/usr/lib/libclang.so'
let g:deoplete#sources#clang#clang_header= '/usr/lib/clang'
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_debug = 1
" let g:deoplete#enable_profile = 1
" Doxygen comment type
let g:DoxygenToolkit_commentType = "C++"

" -----------------------------------------------------------------------------
" Eye Candy
" -----------------------------------------------------------------------------

" vim color scheme
colorscheme onedark
" use terminal gui colors
set termguicolors
" transparent background
highlight Normal guibg=NONE ctermbg=NONE
" the bottom bar theme
let g:lightline = { 'colorscheme': 'onedark' }
" hide --INSERT as the bottom bar displays it
set noshowmode
" split bar theme :split and :vs
autocmd ColorScheme * highlight VertSplit gui=NONE guifg=#2c323c guibg=#2c323c

set scrolloff=3                   " number of screen lines to show around the cursor
set fillchars=vert:\│             " show a continuous line for vsplits
set list listchars=tab:»·,trail:· " show tabs and trailing whitespace
set number                        " show the line number (for the current line)
set relativenumber                " show relative line numbers for each line
set inccommand=nosplit            " show live preview of substitutions

" -----------------------------------------------------------------------------

" F5 to remove all trailing whitespaces
nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>

" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" -----------------------------------------------------------------------------
" DISPLAYING TEXT
" -----------------------------------------------------------------------------

" Write text with no spelling errors
" [s and ]s to move to next spelling error
" z= to get a list of correct spelling
let g:lexical#spell = 1
set spell spelllang=en,fr
augroup lexical
  autocmd!
  autocmd FileType markdown,mkd call lexical#init()
  autocmd FileType textile call lexical#init()
  autocmd FileType lout call lexical#init()
  autocmd FileType text call lexical#init({ 'spell': 0 })
augroup END

let g:tq_language=['en', 'fr', 'pl']

" Align GitHub-flavored Markdown tables
" select table to align with Shift + v
" double press on \ key
au FileType markdown vmap <Leader><Bslash> :EasyAlign*<Bar><Enter>

" Function to create buffer local mappings
fun! AsciidoctorMappings()
  nnoremap <buffer> <leader>oo :AsciidoctorOpenRAW<CR>
  nnoremap <buffer> <leader>op :AsciidoctorOpenPDF<CR>
  nnoremap <buffer> <leader>oh :AsciidoctorOpenHTML<CR>
  nnoremap <buffer> <leader>ch :Asciidoctor2HTML<CR>
  nnoremap <buffer> <leader>cp :Asciidoctor2PDF<CR>
endfun

" Call AsciidoctorMappings for all `*.adoc` and `*.asciidoc` files
augroup asciidoctor
  au!
  au BufEnter *.adoc,*.asciidoc call AsciidoctorMappings()
augroup END

" What to use for HTML, default `asciidoctor`.
let g:asciidoctor_executable = 'asciidoctor'

" What extensions to use for HTML, default `[]`.
let g:asciidoctor_extensions = ['asciidoctor-diagram']

" Path to the custom css
let g:asciidoctor_css_path = ''

" Custom css name to use instead of built-in
let g:asciidoctor_css = ''

" What to use for PDF, default `asciidoctor-pdf`.
let g:asciidoctor_pdf_executable = 'asciidoctor-pdf'

" What extensions to use for PDF, default `[]`.
" let g:asciidoctor_pdf_extensions = ['asciidoctor-diagram', 'asciidoctor-bibtex']
let g:asciidoctor_pdf_extensions = ['asciidoctor-diagram']

" Path to PDF themes, default `''`.
let g:asciidoctor_pdf_themes_path = ''

" Path to PDF fonts, default `''`.
let g:asciidoctor_pdf_fonts_path = ''

nnoremap <leader>cf :let @+ = expand("%:p") \| echo ' > ' . @+<CR>

" -----------------------------------------------------------------------------
" MOVE
" -----------------------------------------------------------------------------

" Magic to align end of line comments and other characters
" Start interactive EasyAlign in visual mode (e.g. vipga)
" vipga= to `v`isual select `i`nner `p`aragraph
"           `ga` to run EasyAlign
"           `=` to align all the = characters
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" Revert this commit https://github.com/neovim/neovim/pull/13268
" I prefer the old behavior 
nnoremap Y yy

" -----------------------------------------------------------------------------
" FUCK UP THE INDENTS
" -----------------------------------------------------------------------------

" Fuck up all the indents
" one indent = 4 spaces
setlocal autoindent
setlocal cindent
setlocal smartindent
set expandtab
set shiftwidth=4

