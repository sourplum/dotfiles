# Kitty terminal kitten to query if a keyboard shortcut is mapped or not
# Copy code to ~/.config/kitty/keymap_query.py
# To run: kitty +kitten keymap_query <keycombo>
# Keycombos are like in kitty config, e.g.: "ctrl+k", "ctrl+shift+f2"...

from kitty.cli import create_default_opts
from kitty.options.utils import parse_shortcut

def main(args):
    query_keystr = args[1]
    query_key = parse_shortcut(query_keystr)

    opts = create_default_opts()
    keymap = opts.keymap

    if keymap.get(query_key):
        action = keymap[query_key]
        print(f"{query_keystr} is mapped to: {action}\n")
        print(f"https://sw.kovidgoyal.net/kitty/actions/#action-{action.split()[0]}")
        print("(this link will only work for keys bound to default mappable actions)")
    else:
        print(f"{query_keystr} is not mapped")
