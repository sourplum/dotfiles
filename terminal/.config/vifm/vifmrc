" vim: filetype=vifm :
" {{{ General config
" This is the actual command used to start vi.  The default is vim.
" If you would like to use another vi clone such as Elvis or Vile
" you will need to change this setting.
set vicmd=nvim

" This makes vifm perform file operations on its own instead of relying on
" standard utilities like `cp`.  While using `cp` and alike is a more universal
" solution, it's also much slower when processing large amounts of files and
" doesn't support progress measuring.
set syscalls

" Open with preview window
view

" Trash Directory
" The default is to move files that are deleted with dd or :d to
" the trash directory.  If you change this you will not be able to move
" files by deleting them and then using p to put the file in the new location.
" I recommend not changing this until you are familiar with vifm.
" This probably shouldn't be an option.
set trash

" This is how many directories to store in the directory history.
set history=1000

" Automatically resolve symbolic links on l or Enter.
set nofollowlinks

" Natural sort of (version) numbers within text.
set sortnumbers

" Maximum number of changes that can be undone.
set undolevels=100

" If you would like to run an executable file when you
" press return on the file name set this.
" set norunexec

" Selected color scheme
colorscheme minimal

" Format for displaying time in file list. For example:
" TIME_STAMP_FORMAT=%m/%d-%H:%M
" See man date or man strftime for details.
" set timefmt=%m/%d\ %H:%M
set timefmt=

" Show list of matches on tab completion in command-line mode
set wildmenu

" Display completions in a form of popup with descriptions of the matches
set wildstyle=popup

" Display suggestions in normal, visual and view modes for keys, marks and
" registers (at most 5 files).  In other view, when available.
set suggestoptions=normal,visual,view,otherpane,keys,marks,registers

" Ignore case in search patterns unless it contains at least one uppercase
" letter
set ignorecase
set smartcase

" Don't highlight search results automatically
set nohlsearch

" Use increment searching (search while typing)
set incsearch

" Try to leave some space from cursor to upper/lower border in lists
set scrolloff=0

" Don't do too many requests to slow file systems
if !has('win')
    set slowfs=curlftpfs
endif

" Things that should be stored in vifminfo
set vifminfo=

" Dont show delete confirmation
" set confirm-=delete

" ------------------------------------------------------------------------------

" :com[mand][!] command_name action
" The following macros can be used in a command
" %a is replaced with the user arguments.
" %c the current file under the cursor.
" %C the current file under the cursor in the other directory.
" %f the current selected file, or files.
" %F the current selected file, or files in the other directory.
" %b same as %f %F.
" %d the current directory name.
" %D the other window directory name.
" %m run the command in a menu window

" command! df df -h %m 2> /dev/null
" command! diff vim -d %f %F
" command! zip zip -r %f.zip %f
" command! run !! ./%f
" command! make !!make %a
" command! mkcd :mkdir %a | cd %a
" command! vgrep vim "+grep %a"
" command! reload :write | restart

" Empty the ruler. By default, it shows the number of directories+files.
set rulerformat=
" }}}


" {{{ File preview & file opening
" The file type is for the default programs to be used with
" a file extension.
" :filetype pattern1,pattern2 defaultprogram,program2
" :fileviewer pattern1,pattern2 consoleviewer
" The other programs for the file type can be accessed with the :file command
" The command macros %f, %F, %d, %F may be used in the commands.
" The %a macro is ignored.  To use a % you must put %%.

" For automated FUSE mounts, you must register an extension with :file[x]type
" in one of following formats:
"
" :filetype extensions FUSE_MOUNT|some_mount_command using %SOURCE_FILE and %DESTINATION_DIR variables
" %SOURCE_FILE and %DESTINATION_DIR are filled in by vifm at runtime.
" A sample line might look like this:
" :filetype *.zip,*.jar,*.war,*.ear FUSE_MOUNT|fuse-zip %SOURCE_FILE %DESTINATION_DIR
"
" :filetype extensions FUSE_MOUNT2|some_mount_command using %PARAM and %DESTINATION_DIR variables
" %PARAM and %DESTINATION_DIR are filled in by vifm at runtime.
" A sample line might look like this:
" :filetype *.ssh FUSE_MOUNT2|sshfs %PARAM %DESTINATION_DIR
" %PARAM value is filled from the first line of file (whole line).
" Example first line for SshMount filetype: root@127.0.0.1:/
"
" You can also add %CLEAR if you want to clear screen before running FUSE
" program.

" Dont show preview on ../ as this confuses me at times
" fileview ../ %pc echo >/dev/null

" All
fileviewer <*>
        \ preview %c:p %px %py %pw %ph %N
        \ %pc
        \ preview clear

" CSV/Excel
filetype *.csv,*.xlsx libreoffice %c %i
fileviewer *.csv sed "s/,,,,/,,-,,/g;s/,,/ /g" %c | column -t | sed "s/ - /  /g" | cut -c -%pw

" HTMLs
fileviewer *.html w3m -dump %c
filextype *.html,*.htm qutebrowser %f 2>/dev/null &

" Playlist
" filextype *.m3u mpv --playlist=%c %i &
filextype *.m3u
    \ mpv --playlist=%c %i &
    \ mpc load %c:r %i &

" Text based files
filetype <text/*> nvim
filextype *.txt,*.json nvim

" Books
filextype *.pdf,*.epub,*.cbz,*.cbr okular %c %i &

" Krita
filextype *.kra krita %c %i &

" Audios
filetype <audio/*> mpv %c %i &

" Videos
filetype <video/*> mpv %c %i &

" Images
filetype <image/*> imv -n %c (find . -maxdepth 0)

" Archives
" fileviewer *.zip,*.jar,*.war,*.ear,*.oxt zip -sf %c
" fileviewer *.tgz,*.tar.gz tar -tzf %c
" fileviewer *.tar.bz2,*.tbz2 tar -tjf %c
" fileviewer *.tar.txz,*.txz xz --list %c
" fileviewer *.tar tar -tf %c
" fileviewer *.rar unrar v %c
" fileviewer *.7z 7z l %c

" Other files
" Using xdg-open to open the highlighted file with a compatible program and
" the reason why I am using "file" to preview other files is so that "vifm"
" does not lag when trying "cat" the file
" filetype * xdg-open %c
" fileviewer * file -b %c
" }}}


"{{{ Key mappings
" Easily quit vifm by hitting q
nmap q ZQ

" Set highlighted image as wallpaper
" nnoremap <C-w> :!bash ~/.config/ranger/scripts/wall.sh %c &<cr>

" Upload highlighted file to 0x0.st and then save url to clipboard
" nnoremap 0x0 :!curl -s -F'file=@%c' https://0x0.st > /dev/null | xclip -sel clip && notify-send "vifm" "File uploaded: $(xclip -o -selection clipboard)" &<cr>

" Reverse image search with Tiney
" nnoremap re :!bash ~/bin/utils/tineye %c &<cr>

" Go to the file that is right before "../" for going to the top most file
nnoremap gg ggj

" Quick shortcuts to some dirs
nnoremap cd :cd<cr>

" Start shell in current directory
nnoremap s :shell<cr>

" Display sorting dialog
nnoremap S :sort<cr>

" Toggle visibility of preview window
" nnoremap w :view<cr>
" noremap w :view<cr>gv

" Open file in nvim
nnoremap o :!nvim %f<cr>

" Open file in the background using its default program
nnoremap gb :file &<cr>l

" Yank current directory path into the clipboard
nnoremap yd :!echo -n %d | xclip -i -selection clipboard %i<cr>

" Yank current file path into the clipboard
nnoremap yf :!echo -n %c:p | xclip -i -selection clipboard %i<cr>

" Mappings for faster renaming
nnoremap I cw<c-a>
nnoremap cc cw<c-u>
nnoremap A cw

" Extract an archive
nnoremap x :!atool -xD %f &<cr>

" Set new wallapaper
nnoremap w :!wallpaper "%c" &<cr>

" Make a new directory
nnoremap mkd :mkdir<space>
"}}}

" Hold shift to jump five files
nnoremap J 5j
nnoremap K 5k

" Just type - or _ to resize the panels
nnoremap - <C-w>5<
nnoremap _ <C-w>5>


"{{{ Icons
" Ranger devicons for ViFM
" https://github.com/cirala/vifm_devicons
"
" Filetypes/directories
set classify=' :dir:/, :exe:, :reg:, :link:,? :?:, ::../::'
set classify+=' ::downloads/::/'
set classify+=' ::pictures/::/'
set classify+=' ::projects/::/'
set classify+=' ::music/::/'
set classify+='辶::videos/,,nico/::/'
set classify+=' ::movies/::/'
set classify+='龎::books/::/'
set classify+=' ::dotfiles/::/'
set classify+=' ::sailfish/::/'
set classify+=' ::games/,,roms/::/'
set classify+=' ::krita/::/'
set classify+=' ::tags/::/'

" Specific files
set classify+=' ::.Xdefaults,,.Xresources,,.bashprofile,,.bash_profile,,.bashrc,,.dmrc,,.d_store,,.fasd,,.gitconfig,,.gitignore,,.jack-settings,,.mime.types,,.nvidia-settings-rc,,.pam_environment,,.profile,,.recently-used,,.selected_editor,,.xinitpurc,,.zprofile,,.yarnc,,.snclirc,,.tmux.conf,,.urlview,,.config,,.ini,,.user-dirs.dirs,,.mimeapps.list,,.offlineimaprc,,.msmtprc,,.Xauthority,,config::'
set classify+=' ::dropbox::'
set classify+=' ::favicon.*,,README,,readme::'
set classify+=' ::.vim,,.vimrc,,.gvimrc,,.vifm::'
set classify+=' ::gruntfile.coffee,,gruntfile.js,,gruntfile.ls::'
set classify+=' ::gulpfile.coffee,,gulpfile.js,,gulpfile.ls::'
set classify+=' ::ledger::'
set classify+=' ::license,,copyright,,copying,,LICENSE,,COPYRIGHT,,COPYING::'
set classify+=' ::node_modules::'
set classify+=' ::react.jsx::'

" File extensions
set classify+='λ ::*.ml,,*.mli::'
set classify+=' ::*.styl::'
set classify+=' ::*.scss::'
set classify+=' ::*.py,,*.pyc,,*.pyd,,*.pyo::'
set classify+=' ::*.php::'
set classify+=' ::*.markdown,,*.md::'
set classify+=' ::*.json::'
set classify+=' ::*.js::'
set classify+=' ::*.bmp,,*.gif,,*.ico,,*.jpeg,,*.jpg,,*.png,,*.svg,,*.svgz,,*.tga,,*.tiff,,*.xmb,,*.xcf,,*.xpm,,*.xspf,,*.xwd,,*.cr2,,*.dng,,*.3fr,,*.ari,,*.arw,,*.bay,,*.crw,,*.cr3,,*.cap,,*.data,,*.dcs,,*.dcr,,*drf,,*.eip,,*.erf,,*.fff,,*.gpr,,*.iiq,,*.k25,,*.kdc,,*.mdc,,.*mef,,*.mos,,.*.mrw,,.*.obm,,*.orf,,*.pef,,*.ptx,,*.pxn,,*.r3d,,*.raf,,*.raw,,*.rwl,,*.rw2,,*.rwz,,*.sr2,,*.srf,,*.srf,,*.srw,,*.tif,,*.x3f,,*.webp::'
set classify+=' ::*.ejs,,*.htm,,*.html,,*.slim,,*.xml::'
set classify+=' ::*.mustasche::'
set classify+=' ::*.css,,*.less,,*.bat,,*.conf,,*.ini,,*.rc,,*.yml,,*.cfg,,*.rc::'
set classify+=' ::*.rss::'
set classify+=' ::*.coffee::'
set classify+=' ::*.twig::'
set classify+=' ::*.c++,,*.cc,,*.c,,*.cpp,,*.cxx,,*.c,,*.h::'
set classify+=' ::*.hs,,*.lhs::'
set classify+=' ::*.lua::'
set classify+=' ::*.jl::'
set classify+=' ::*.go::'
set classify+=' ::*.ts::'
set classify+=' ::*.db,,*.dump,,*.sql::'
set classify+=' ::*.sln,,*.suo::'
set classify+=' ::*.exe::'
set classify+=' ::*.diff,,*.sum,,*.md5,,*.sha512::'
set classify+=' ::*.scala::'
set classify+=' ::*.java,,*.jar::'
set classify+=' ::*.xul::'
set classify+=' ::*.clj,,*.cljc::'
set classify+=' ::*.pl,,*.pm,,*.t::'
set classify+=' ::*.cljs,,*.edn::'
set classify+=' ::*.rb::'
set classify+=' ::*.fish,,*.sh,,*.bash::'
set classify+=' ::*.dart::'
set classify+=' ::*.f#,,*.fs,,*.fsi,,*.fsscript,,*.fsx::'
set classify+=' ::*.rlib,,*.rs::'
set classify+=' ::*.d::'
set classify+=' ::*.erl,,*.hrl::'
set classify+=' ::*.ai::'
set classify+=' ::*.psb,,*.psd::'
set classify+=' ::*.jsx::'
set classify+=' ::*.vim,,*.vimrc::'
set classify+=' ::*.aac,,*.anx,,*.asf,,*.au,,*.axa,,*.flac,,*.m2a,,*.m4a,,*.mid,,*.midi,,*.mp3,,*.mpc,,*.oga,,*.ogg,,*.ogx,,*.ra,,*.ram,,*.rm,,*.spx,,*.wav,,*.wma,,*.ac3::'
set classify+=' ::*.avi,,*.flv,,*.mkv,,*.mov,,*.mov,,*.mp4,,*.mpeg,,*.mpg,,*.webm::'
set classify+=' ::*.epub,,*.pdf,,*.fb2,,*.djvu::'
set classify+=' ::*.7z,,*.apk,,*.bz2,,*.cab,,*.cpio,,*.deb,,*.gem,,*.gz,,*.gzip,,*.lh,,*.lzh,,*.lzma,,*.rar,,*.rpm,,*.tar,,*.tgz,,*.xz,,*.zip::'
set classify+=' ::*.cbr,,*.cbz::'
set classify+=' ::*.log::'
set classify+=' ::*.doc,,*.docx,,*.adoc::'
set classify+=' ::*.xls,,*.xls,,*.xlsmx::'
set classify+=' ::*.pptx,,*.ppt::'
"}}}

windo set sort=+mtime,dir
windo set sortorder=descending

windo filter go/|quicklisp/|Desktop/|Ansible/|mail/|^.*\.kra~\/?$

" Ellipsis on title
set viewcolumns=-{name}..

set grepprg='ag --line-numbers %i %a %s'

highlight User1 ctermbg=red
highlight User2 ctermbg=blue ctermfg=white cterm=bold
set statusline="%1* %-26t %2* %= %1* %-5s"

set title
set iec

" autocmd DirEnter ~/Videos/**,~/Pictures/manga/**,~/Books/**,~/Movies/** setlocal sort=+name,+mtime,-dir
autocmd DirEnter ~/Movies/** setlocal sort=+name,+mtime,-dir
autocmd DirEnter ~/Videos/**,~/Movies/** filter .ass|.srt

" drag and drop
command! dragon dragon-drag-and-drop -a -x %f
nmap <C-d> :dragon<CR>
