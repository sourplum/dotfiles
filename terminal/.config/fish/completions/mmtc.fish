complete -c mmtc -n "__fish_use_subcommand" -l address -d 'Specify the address of the mpd server' -r
complete -c mmtc -n "__fish_use_subcommand" -s c -l config -d 'Specify the config file' -r
complete -c mmtc -n "__fish_use_subcommand" -l jump-lines -d 'The number of lines to jump' -r
complete -c mmtc -n "__fish_use_subcommand" -l seek-secs -d 'The time to seek in seconds' -r
complete -c mmtc -n "__fish_use_subcommand" -l ups -d 'The amount of status updates per second' -r
complete -c mmtc -n "__fish_use_subcommand" -l clear-query-on-play -d 'Clear query on play'
complete -c mmtc -n "__fish_use_subcommand" -l cycle -d 'Cycle through the queue'
complete -c mmtc -n "__fish_use_subcommand" -l no-clear-query-on-play -d 'Don\'t clear query on play'
complete -c mmtc -n "__fish_use_subcommand" -l no-cycle -d 'Don\'t cycle through the queue'
complete -c mmtc -n "__fish_use_subcommand" -s h -l help -d 'Prints help information'
complete -c mmtc -n "__fish_use_subcommand" -s V -l version -d 'Prints version information'
