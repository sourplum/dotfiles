function current_video
    python3 -c "import sys, urllib.parse as ul; print(ul.unquote_plus(sys.argv[1]))" (playerctl -p mpv metadata xesam:url | sed 's#^file://##')
end
