function display_tags --description 'Display files stored in tag files'
    set -l choice (ls ~/tags | rofi -dmenu -fullscreen -p " tag: ")
    if test -n "$choice"
        imv echo (tac ~/tags/$choice)
    end
end
