# Defined in /tmp/fish.OpTIaw/mangajar.fish @ line 2
function mangajar --description 'Extractor for mangajar.com'
  set -l slug "$argv[1]"
  set -l chapter (echo $argv[2] | bc)

  set -l pages (curl -X GET "https://mangajar.com/manga/$slug/chapter/$chapter" -s | sed -n 's#.*"\(https://static.mangajar.com/pages.*\.jpg\)".*#\1#p' | tail -n+2)
  set -l lastpage (echo $pages | tr ' ' '\n' | wc -l)

  for index in (seq -w 1 $lastpage)
    # echo "Downloading page $index with link $pages[$index]"
    curl -o "$index.jpg" $pages[$index]
  end
end
