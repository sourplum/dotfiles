function current_wallpaper
    awk -F "'" '{print $2}' ~/.fehbg | tr -d "\n"
end
