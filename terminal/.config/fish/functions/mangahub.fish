# Defined in /tmp/fish.rW8s8Q/mangahub.fish @ line 2
function mangahub --description 'Extractor for mangahub.io'
  set -l slug "$argv[1]"
  set -l chapter (echo $argv[2] | bc)

  set -l lastpage (curl -X GET https://mangahub.io/chapter/$slug/chapter-$chapter -s | grep -o '/[[:digit:]]*</p><img' | head -1 | grep -o '[[:digit:]]*' | bc)
  seq 1 $lastpage | xargs -I% curl -O "https://img.mghubcdn.com/file/imghub/$slug/$chapter/%.jpg"
end
