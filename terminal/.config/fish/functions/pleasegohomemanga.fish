# Defined in /tmp/fish.VhPXQY/pleasegohomemanga.fish @ line 2
function pleasegohomemanga --description 'Extractor for pleasegohomemanga.com'
  set -l slug "$argv[1]"
  set -l chapter (echo $argv[2] | bc)

  if test \( $chapter -ge 4 \) -a \( $chapter -le 38 \)
    curl -X GET "https://pleasegohomemanga.com/manga/$slug-vol-1-chapter-$chapter/" -s | sed -n "s#.*\"\(https://cdn.bakihanma.com/file/Zolyvmanga/$slug/vol-1-chapter-$chapter/.*\.jpg\)\".*#\1#p" | uniq | xargs -I% curl -O '%'
  else
    curl -X GET "https://pleasegohomemanga.com/manga/$slug-chapter-$chapter/" -s | sed -n "s#.*\"\(https://cdn.bakihanma.com/file/Zolyvmanga/$slug/chapter-$chapter/.*\.jpg\)\".*#\1#p" | uniq | xargs -I% curl -O '%'
  end
end
