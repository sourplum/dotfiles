function rounded_wallpaper
    rounded_borders.sh (cat ~/.fehbg | tail -n1 | sed -r 's/.*\'(.*)\'/\1/g' | sed -r 's/\s$//g')
end
