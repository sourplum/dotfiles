function display_bookmarks --description 'Display web bookmarks and open it with qutebrowser'
    set -l choice (cat ~/links/(ls ~/links/ | rofi -dmenu -fullscreen -p " bookmark: ") | rofi -dmenu -fullscreen -p " url: ")
    if test -n "$choice"
        qutebrowser $choice
    end
end
