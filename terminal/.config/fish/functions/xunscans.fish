function xunscans --description 'Extractor for xunscans.xyz'
  set -l slug "$argv[1]"
  set -l chapter (echo $argv[2] | bc)

  curl -XGET "https://xunscans.xyz/manga/$slug/chapter-$chapter/" -s | sed -n "s#\(https://xunscans.xyz/wp-content/uploads/WP-manga/data.*\)\" class.*#\1#p" | sed 's#\s##g' | xargs -I% curl -O '%'
end
