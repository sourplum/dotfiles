function last_file
    set -l depth "$argv[2]"
    test -z "$depth"; and set -l depth 1
    ls -d -tr $argv[1]/* | tail -n"$depth"
end
