# Defined in /tmp/fish.l2GZkQ/mangaread.fish @ line 2
function mangaread --description 'Extractor for mangaread.co'
  set -l slug $argv[1]
  set -l chapter (echo $argv[2] | bc)

  curl -X GET "https://mangaread.co/manga/$slug/ch-$chapter" -s | grep -o "var chapter_preloaded_images.*" | sed -n "s#.*\[\(.*\)\].*#\1#p" | tr ',' '\n' | jq . | xargs -I% curl -O '%'
end
