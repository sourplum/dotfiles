# Defined in /tmp/fish.Jm51fq/catmanga.fish @ line 2
function catmanga --description 'Extractor for catmanga.org'
  set -l slug "$argv[1]"
  set -l chapter (echo $argv[2] | bc)

  curl -X GET "https://catmanga.org/series/$slug/$chapter" -s | sed -n 's#.*"pages":\[\(.*\)\],.*#\1#p' | sed 's#,#\n#g' | xargs -I% curl -O '%'
end
